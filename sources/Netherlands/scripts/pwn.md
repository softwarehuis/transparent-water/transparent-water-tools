Should scrape https://www.pwn.nl/overzichtskaart, leaflet markers are hidden in CDATA object. Need to parse this object, convert to javascript object and scrape all the individual points.

Quality reports at https://www.pwn.nl/samenstelling-van-het-drinkwater

Map of zones, needs to be parsed manually https://www.pwn.nl/sites/default/files/pwn_pompstations_def.jpg
