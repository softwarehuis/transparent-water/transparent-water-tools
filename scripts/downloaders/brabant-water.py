import requests
from bs4 import BeautifulSoup as bs

url = "https://www.brabantwater.nl/sites/brabantwater.nl/files/documents/Waterkwaliteitsgegevens/2018/Q4/Bergen%20op%20Zoom%20Jaaroverzicht%20.pdf"

r = requests.get(url)
soup = bs(r.text, "html.parser")
print(r.text)

for i, link in enumerate(soup.findAll('a')):
    _url = url + link.get('href')

    for x in range(i):
        output = open('file[%d].pdf' % x, 'wb')
        output.write(url.read())
        output.close()
